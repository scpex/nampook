const http = require("http");
const config = require("../config.json");

function callHarperDB(operation, callback) {
  let protocol = config.protocol + ":";    
  let options = {
    method: "POST",
    protocol: `${protocol}`,
    hostname: `${config.host}`,
    port: config.port,
    path: "/",
    headers: {
      "content-type": "application/json",
      authorization:
        "Basic " +
        Buffer.from(config.username + ":" + config.password).toString(
          "base64"
        ),
      "cache-control": "no-cache"
    }
  };  

  let http_req = http.request(options, function(hdb_res) {
    let chunks = [];

    hdb_res.on("data", function(chunk) {
      chunks.push(chunk);
    });

    hdb_res.on("end", function() {
      let body = Buffer.concat(chunks);
      if (isJson(body) && hdb_res.statusCode === 200) {
        return callback(null, JSON.parse(body), hdb_res.statusCode, hdb_res.headers['x-response-time']);
      } else {
        return callback(body.toString(), null);
      }
    });
  });

  http_req.on("error", function(chunk) {
    return callback("Failed to connect", null);
  });

  http_req.write(JSON.stringify(operation));
  http_req.end();
}

function isJson(s) {
  try {
    JSON.parse(s);
    return true;
  } catch (e) {
    return false;
  }
}

module.exports = {
  callHarperDB: callHarperDB
};