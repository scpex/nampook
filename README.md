# nampook 

[![Version](https://img.shields.io/npm/v/@heroku-cli/plugin-status.svg)](https://npmjs.org/package/@heroku-cli/plugin-status)
[![Build Status](https://travis-ci.org/scpex/nampook.svg?branch=master)](https://travis-ci.org/scpex/nampook)
[![pipeline status](https://gitlab.com/scpex/nampook/badges/master/pipeline.svg)](https://gitlab.com/scpex/nampook/commits/master)
[![Heroku App Status](http://heroku-shields.herokuapp.com/gitlab-nodejs)](https://gitlab-nodejs.herokuapp.com)

nodejs
expressjs
harperdb
mongodb

credit heroku status markdown from http://heroku-shields.herokuapp.com/