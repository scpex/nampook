var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var nampookRouter = require('./routes/nampook');
var khunkhuanRouter = require('./routes/khunkhuan');
var pongRouter = require('./routes/pong');
var phayaoRouter = require('./routes/phayao');
var lifestyleRouter = require('./routes/lifestyle');
var attractionRouter = require('./routes/attraction');
var stayRouter = require('./routes/stay');
var galleryRouter = require('./routes/gallery');
var videoRouter = require('./routes/video');
var contactRouter = require('./routes/contact');
var testRouter = require('./routes/test')

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/index', indexRouter);
app.use('/nampook', nampookRouter);
app.use('/khunkhuan', khunkhuanRouter);
app.use('/pong', pongRouter);
app.use('/phayao', phayaoRouter);
app.use('/lifestyle', lifestyleRouter);
app.use('/attraction', attractionRouter);
app.use('/stay', stayRouter);
app.use('/gallery', galleryRouter);
app.use('/video', videoRouter);
app.use('/contact', contactRouter);
app.use('/test', testRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
