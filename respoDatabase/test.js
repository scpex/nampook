const { promisify } = require("util");
const call_harperdb = require("./../utility/callHarperdb");
const p_call_harperdb = promisify(call_harperdb.callHarperDB);

module.exports = {
  test: test
};

async function test(uid) {
  var QUERY = {
    operation: "sql",
    sql: "SELECT * FROM thailand.municipal where id="+uid
  };

  return await p_call_harperdb(QUERY);
}
