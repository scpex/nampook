var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('pong', { title: 'Pong' });
});

module.exports = router;
