var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('stay', { title: 'Stay' });
});

module.exports = router;
